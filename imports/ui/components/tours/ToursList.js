import React from 'react';
import { ListGroup, ListGroupItem, Alert } from 'react-bootstrap';

const ToursList = ({ tours }) => (
  tours.length > 0 ? <ListGroup className="ToursList">
    {tours.map(({ _id, title }) => (
      <ListGroupItem key={ _id } href={`/tours/${_id}`}>{ title }</ListGroupItem>
    ))}
  </ListGroup> :
  <Alert bsStyle="warning">No tours yet.</Alert>
);

ToursList.propTypes = {
  tours: React.PropTypes.array,
};

export default ToursList;
