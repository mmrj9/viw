/* eslint-disable max-len, no-return-assign */

import React from 'react';
import { FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import tourEditor from '../../../modules/tour-editor.js';
import ImageDropZone from './TourImageUpload.js';

export default class TourEditor extends React.Component {

  constructor(props){
    super(props);
    this.state = {"files": []};
    this.updateFiles = this.updateFiles.bind(this);
  }

  componentDidMount() {
    tourEditor({ component: this });
    setTimeout(() => { document.querySelector('[name="title"]').focus(); }, 0);
  }

  updateFiles(files) {
      this.setState({
          files: files
      });
  }

  render() {
    const { tour } = this.props;
    return (<form
      ref={ form => (this.tourEditorForm = form) }
      onSubmit={ event => event.preventDefault() }
    >
      <FormGroup>
        <ControlLabel>Title</ControlLabel>
        <FormControl
          type="text"
          name="title"
          defaultValue={ tour && tour.title }
          placeholder="Apartment in Lisbon"
        />
      </FormGroup>
      <FormGroup>
        <ControlLabel>Description</ControlLabel>
        <FormControl
          componentClass="textarea"
          name="description" 
          id="description"
          defaultValue={ tour && tour.description }
          placeholder="A really nice tour around a new apartment in Lisbon"
        />
      </FormGroup>
      <ImageDropZone files={this.state.files} updateFiles={this.updateFiles} />
      <div style={{"textAlign":"right"}}>
      <Button type="submit" bsStyle="success" style={{"marginTop":"20px"}}>
        { tour && tour._id ? 'Save Changes' : 'Add Tour' }
      </Button>
      </div>
    </form>);
  }
}

TourEditor.propTypes = {
  tour: React.PropTypes.object,
};
