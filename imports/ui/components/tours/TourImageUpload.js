import React from 'react';
import Dropzone from 'react-dropzone';
//import { Button } from 'react-bootstrap';



//Bytes of 5MB
const FIVE_MiB = 5242880; 

const dropzoneStyle = {
  	"width": "100%",
  	"borderWidth": "2px",
    "borderColor": "rgb(102, 102, 102)",
    "borderStyle": "dashed",
    "borderRadius": "5px",
    "minHeight": "100px"
};

const imagePreviewStyle = {
	"width": '200px', 
	"height": '100px', 
	"marginLeft": '10px',
	"marginTop": '10px',
	"marginBottom": '10px'
};

const divPreviewStyle = {
	"width": '200px', 
	"height": '100px',
	"display": 'inline',
	"position": "relative"
}

const removeImageStyle = {
	"position": "absolute",
  	"top": "-47px",
  	"left": "187px",
  	"zIndex": "10"
}

const crossStyle = {
	"fontSize": "35px",
	"color": "white",
	"opacity": "0.6"
}

class Preview extends React.Component {

	constructor(props) {
		super(props);
	}
	
	render(){
		var res;
    if(this.props.files && this.props.files.length > 0){
      res = (<div className="text-center">
           {this.props.files.map(function(file, i){
       			return <div style={divPreviewStyle} key={i}>
					<div style={removeImageStyle}>
						<button className="close" style={crossStyle} value={i} onClick={this.props.remove}>&times;</button>
					</div>
					<img style={imagePreviewStyle} src={file.preview}/>
       			 </div>
      		}.bind(this)
      		)}	
      </div>
      )    
  	} 
  	else {
      res = (
      	<div className="text-center" style={{"marginTop":"25px"}}>
      		<div style={{ "fontSize": "25px"}}>{this.props.text}
      		</div>
      	</div>)
  	}
  	return res;
  }
};

export default class ImageDropZone extends React.Component {
    constructor(props) {
      super(props);
      this.maxSize = props.maxSize ? props.maxSize : FIVE_MiB;
      this.state = {
        files: null
      }
      this.remove = this.remove.bind(this);
    }

    onDrop(files) {
      if (files.length === 0) {
        Bert.alert('Invalid file(s)!', 'danger');
        return;
      }

     var temp = new Array();

     if(this.state.files != null)
        temp = this.state.files;

    Array.prototype.contains = function(obj) {
	    var i = this.length;
	    while (i--) {
	        if (this[i].name === obj.name) {
	            return true;
	        }
	    }
	    return false;
	}

	files.forEach(function(file) {
	      if (file.size > this.maxSize) {
	        Bert.alert(`File: ${file.name} size: ${(file.size/ 1048576).toFixed(1)} MB > max: ${(this.maxSize/ 1048576)} MB`, 'danger');
	        return;
	      }
	      else {
	      	if(!temp.contains(file)){
	      		temp.push(file);
	      		return;
	      	}
	      	else{
	      		Bert.alert(`Duplicated File`, 'warning');
	      		return;
	      	}
	      }
	 }.bind(this));

      this.setState({
          files: temp
      });
      this.props.updateFiles(temp);
    }

    remove(event) {
    	event.preventDefault();
    	event.stopPropagation();
    	temp = this.state.files;
    	temp.splice(event.target.value,1);
      	this.setState({
          files: temp
      	});

    }

    render() {
      return (
        <Dropzone multiple={true} style={dropzoneStyle}  onDrop={this.onDrop.bind(this)} accept="image/*">
        	<Preview files={this.state.files} remove={this.remove} text="Drop your images here!"/>
        </Dropzone>
      );
    }
  }