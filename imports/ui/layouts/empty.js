import React from 'react';

const EmptyLayout = ({ children }) => (
  <div>
	{ children }
  </div>
);

EmptyLayout.propTypes = {
  children: React.PropTypes.node,
};

export default EmptyLayout;
