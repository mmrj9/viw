import { composeWithTracker } from 'react-komposer';
import { Meteor } from 'meteor/meteor';
import Tours from '../../../api/tours/tours.js';
import ToursList from '../../components/tours/ToursList.js';
import Loading from '../../components/Loading.js';

const composer = (params, onData) => {
  const subscription = Meteor.subscribe('tours.list',Meteor.userId());
  if (subscription.ready()) {
    const tours = Tours.find().fetch();
    onData(null, { tours });
  }
};

export default composeWithTracker(composer, Loading)(ToursList);
