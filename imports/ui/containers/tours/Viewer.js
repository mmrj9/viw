import { Meteor } from 'meteor/meteor';
import { composeWithTracker } from 'react-komposer';
import TourData from '../../../api/tours/tourdata.js';
import Viewer from '../../pages/tours/Viewer.js';
import Loading from '../../components/Loading.js';

const composer = ({ params }, onData) => {
  const subscription = Meteor.subscribe('tourdata.bytour', params._id);

  if (subscription.ready()) {
    const tourdata = TourData.findOne();
    onData(null, { tourdata });
  }
};

export default composeWithTracker(composer, Loading)(Viewer);
