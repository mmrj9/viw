import { Meteor } from 'meteor/meteor';
import { composeWithTracker } from 'react-komposer';
import Tours from '../../../api/tours/tours.js';
import EditTour from '../../pages/tours/EditTour.js';
import Loading from '../../components/Loading.js';

const composer = ({ params }, onData) => {
  const subscription = Meteor.subscribe('tours.view', params._id);

  if (subscription.ready()) {
    const tour = Tours.findOne();
    onData(null, { tour });
  }
};

export default composeWithTracker(composer, Loading)(EditTour);
