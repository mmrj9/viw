import React from 'react';
import { Jumbotron } from 'react-bootstrap';

const Index = () => (
  <div className="Index">
    <Jumbotron className="text-center">
      <h2>Viw</h2>
      <p>Build Your Virtual Tours</p>
      <p><a className="btn btn-success" href="/demo" role="button">Demo</a></p>
      <p style={ { fontSize: '16px', color: '#aaa' } }>Currently at v0.0.0</p>
    </Jumbotron>
  </div>
);

export default Index;
