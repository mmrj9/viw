import React from 'react';

export default class Viewer extends React.Component {

        constructor(props) {
            super(props);
            this.state = {
                enabled: false,
                isVR: false,
                Marzipano: null,
                deviceOrientationControlMethod: null,
                view: null,
                controls: null

            };

            // This binding is necessary to make `this` work in the callback
            this.toggleDO = this.toggleDO.bind(this);
            this.toggleVR = this.toggleVR.bind(this);
            //this.data is easier to code with
            this.data = this.props.tourdata;
        }

        componentDidMount() {

            //Hide the device orientation toggle
            $('#toggleDeviceOrientation').hide();

            //Show the device orientation toggle only if device has gyroscope sensor
            window.addEventListener("devicemotion", function(event) {
                if ((event.rotationRate.alpha || event.rotationRate.beta || event.rotationRate.gamma)) {
                    $("#toggleDeviceOrientation").show();
                }
            });

            //Load all the scripts
            /**
              TODO: Migrate scripts to own server
            **/
            $.getScript('http://miguelmorujao.com/scripts/WakeLock.js');
            $.getScript('http://www.marzipano.net/demos/common/es5-shim.js', function() {
                $.getScript('http://www.marzipano.net/demos/common/eventShim.js', function() {
                    $.getScript('http://www.marzipano.net/demos/common/requestAnimationFrame.js', function() {
                        $.getScript('http://miguelmorujao.com/scripts/three.js', function() {
                            $.getScript('http://miguelmorujao.com/scripts/webvr-polyfill.js', function() {
                                $.getScript('http://miguelmorujao.com/scripts/es6-promise.js', function() {
                                    $.getScript('http://miguelmorujao.com/scripts/marzipano.js', function() {
                                        $.getScript('http://miguelmorujao.com/scripts/DeviceOrientationControlMethod.js', function() {
                                            var panoElement = document.getElementById('pano');

                                            var viewerOpts = {
                                                controls: {
                                                    mouseViewMode: 'drag' // drag|qtvr
                                                }
                                            };


                                            var viewer = new Marzipano.Viewer(panoElement, viewerOpts)

                                            //***** DEVICE ORIENTATION ******//

                                            //register device orientation controls in viewer controls
                                            this.setState({
                                                deviceOrientationControlMethod: new DeviceOrientationControlMethod()
                                            });
                                            this.setState({
                                                controls: viewer.controls()
                                            });
                                            this.state.controls.registerMethod('deviceOrientation', this.state.deviceOrientationControlMethod);

                                            //***** DEVICE ORIENTATION END ******//

                                              // Create scenes.
                                            var scenes = this.data.scenes.map(function(sceneData) {
                                                var urlPrefix = "http://s3-eu-west-1.amazonaws.com/viw/tours/"+this.data.tourId;
                                                var source = Marzipano.ImageUrlSource.fromString(
                                                  urlPrefix + "/" + sceneData.id + ".jpg");
                                                
                                                var geometry = new Marzipano.EquirectGeometry([{ "width": sceneData.width }]);

                                                var limiter = Marzipano.RectilinearView.limit.traditional(sceneData.width/4, 100*Math.PI/180);
                                                var view = new Marzipano.RectilinearView(sceneData.initialViewParameters, limiter);

                                                var marzipanoScene = viewer.createScene({
                                                  source: source,
                                                  geometry: geometry,
                                                  view: view,
                                                  pinFirstLevel: true
                                                });

                                                return {
                                                  data: sceneData,
                                                  marzipanoObject: marzipanoScene
                                                };
                                            }.bind(this));

                                          // Display the initial scene.
                                          scenes[0].marzipanoObject.switchTo();

                                        }.bind(this));
                                    }.bind(this));
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));

            $.getScript('http://miguelmorujao.com/scripts/attribution.js', function() {
                attribution('Viw by', 'Olloplus', 'http://olloplus.com/en/homepage/');
            });

        }

        componentDidUpdate() {
            $.getScript('http://miguelmorujao.com/scripts/Detector.js', function() {

                if (this.state.isVR && Detector.webgl) {

                    var mat4 = Marzipano.dependencies.glMatrix.mat4;
                    var quat = Marzipano.dependencies.glMatrix.quat;

                    var degToRad = Marzipano.util.degToRad;

                    var viewerElement = document.querySelector("#pano");
                    var enterVrElement = document.querySelector("#toggleVR");
                    var noVrElement = document.querySelector("#no-vr");

                    // Create stage and register renderers.
                    var stage = new Marzipano.WebGlStage();
                    Marzipano.registerDefaultRenderers(stage);

                    // Insert stage into the DOM.
                    viewerElement.appendChild(stage.domElement());

                    // Update the stage size whenever the window is resized.
                    stage.updateSize();
                    window.addEventListener('resize', function() {
                        stage.updateSize();
                    });

                    // Create and start the render loop.
                    var renderLoop = new Marzipano.RenderLoop(stage);
                    renderLoop.start();

                    // Create geometry.
                    var geometry = new Marzipano.CubeGeometry([{
                        tileSize: 256,
                        size: 256,
                        fallbackOnly: true
                    }, {
                        tileSize: 512,
                        size: 512
                    }, {
                        tileSize: 512,
                        size: 1024
                    }, {
                        tileSize: 512,
                        size: 2048
                    }, {
                        tileSize: 512,
                        size: 4096
                    }]);

                    // Create view.
                    var limiter = Marzipano.RectilinearView.limit.traditional(4096, 110 * Math.PI / 180);
                    var viewLeft = new Marzipano.RectilinearView(null, limiter);
                    var viewRight = new Marzipano.RectilinearView(null, limiter);

                    // Create layers.
                    var layerLeft = createLayer(stage, viewLeft, geometry, 'left', {
                        relativeWidth: 0.5,
                        relativeX: 0
                    });
                    var layerRight = createLayer(stage, viewRight, geometry, 'right', {
                        relativeWidth: 0.5,
                        relativeX: 0.5
                    });

                    // Add layers into stage.
                    stage.addLayer(layerLeft);
                    stage.addLayer(layerRight);

                    // Query browser for VR devices.
                    var vrDevices = null;
                    getHMD().then(function(detectedDevices) {
                        vrDevices = detectedDevices;

                        // Set the projection center.
                        if (vrDevices.hmd) {
                            setProjectionCenter(viewLeft, vrDevices.hmd.getEyeParameters("left"));
                            setProjectionCenter(viewRight, vrDevices.hmd.getEyeParameters("right"));
                        }

                        // Update view on movements.
                        if (vrDevices.positionSensor) {
                            requestAnimationFrame(syncViewWithPositionSensor);
                        }

                       // enterVrElement.style.display = vrDevices.hmd ? 'block' : 'none';
                        noVrElement.style.display = vrDevices.hmd ? 'none' : 'block';
                    });

                    // Lock the screen orientation.
                    if (screen.orientation && screen.orientation.lock) {
                        screen.orientation.lock('landscape');
                    }

                    // Prevent display from sleeping on mobile devices.
                    var wakeLock = new WakeLock();
                    wakeLock.request();

                    // Enter fullscreen mode when available.
                    enterVrElement.addEventListener('click', function() {
                        if (viewerElement.mozRequestFullScreen) {
                            viewerElement.mozRequestFullScreen({
                                vrDisplay: vrDevices.hmd
                            });
                        } else if (viewerElement.webkitRequestFullscreen) {
                            console.log("vrDisplay", vrDevices.hmd);
                            viewerElement.webkitRequestFullscreen({
                                vrDisplay: vrDevices.hmd
                            });
                        }
                    });

                    function createLayer(stage, view, geometry, eye, rect) {
                        var urlPrefix = "//www.marzipano.net/media/music-room";
                        var source = new Marzipano.ImageUrlSource.fromString(
                            urlPrefix + "/" + eye + "/{z}/{f}/{y}/{x}.jpg", {
                                cubeMapPreviewUrl: urlPrefix + "/" + eye + "/preview.jpg"
                            });

                        var textureStore = new Marzipano.TextureStore(geometry, source, stage);
                        var layer = new Marzipano.Layer(stage, source, geometry, view, textureStore, {
                            effects: {
                                rect: rect
                            }
                        });

                        layer.pinFirstLevel();

                        return layer;
                    }

                    function setProjectionCenter(view, eyeParameters) {
                        var fovs = eyeParameters.recommendedFieldOfView;

                        var left = degToRad(fovs.leftDegrees),
                            right = degToRad(fovs.rightDegrees),
                            up = degToRad(fovs.upDegrees),
                            down = degToRad(fovs.downDegrees);

                        var hfov = left + right;
                        var offsetAngleX = left - hfov / 2;
                        var projectionCenterX = Math.tan(offsetAngleX) / (2 * Math.tan(hfov / 2));

                        var vfov = up + down;
                        var offsetAngleY = up - vfov / 2;
                        var projectionCenterY = Math.tan(offsetAngleY) / (2 * Math.tan(vfov / 2));

                        view.setParameters({
                            projectionCenterX: projectionCenterX,
                            projectionCenterY: projectionCenterY,
                            fov: vfov
                        });
                    }

                    var positionSensorQuartenion = quat.create();
                    var positionSensorMatrix = mat4.create();
                    var positionSensorParameters = {};

                    function syncViewWithPositionSensor() {
                        var state = vrDevices.positionSensor.getState();

                        if (state.hasOrientation) {
                            if (state.orientation) {
                                var o = state.orientation;
                                quat.set(positionSensorQuartenion, o.x, o.y, o.z, o.w);
                                mat4.fromQuat(positionSensorMatrix, positionSensorQuartenion);

                                eulerFromMat4(positionSensorMatrix, positionSensorParameters);

                                var parameters = {
                                    yaw: -positionSensorParameters._y,
                                    pitch: -positionSensorParameters._x,
                                    roll: -positionSensorParameters._z
                                };

                                viewLeft.setParameters(parameters);
                                viewRight.setParameters(parameters);
                            }
                        }
                        requestAnimationFrame(syncViewWithPositionSensor);
                    }

                    function getHMD() {
                        return new Promise(function(resolve, reject) {
                            var detectedHmd = null;
                            var detectedPositionSensor = null;

                            navigator.getVRDevices().then(function(devices) {
                                var i;
                                for (i = 0; i < devices.length; i++) {
                                    if (devices[i] instanceof HMDVRDevice) {
                                        detectedHmd = devices[i];
                                        break;
                                    }
                                }
                                for (i = 0; i < devices.length; i++) {
                                    if (devices[i] instanceof PositionSensorVRDevice) {
                                        detectedPositionSensor = devices[i];
                                        break;
                                    }
                                }
                                resolve({
                                    hmd: detectedHmd,
                                    positionSensor: detectedPositionSensor
                                });
                            }, function() {
                                resolve({});
                            });
                        });
                    }

                    // Adapted from Three.js
                    // https://github.com/mrdoob/three.js/blob/master/src/math/Euler.js
                    // Assumes the upper 3x3 of m is a pure (unscaled) rotation matrix.
                    function eulerFromMat4(m, result) {
                        var m11 = m[0],
                            m12 = m[4],
                            m13 = m[8];
                        var m21 = m[1],
                            m22 = m[5],
                            m23 = m[9];
                        var m31 = m[2],
                            m32 = m[6],
                            m33 = m[10];

                        var m23clamped = ((m23 < -1) ? -1 : ((m23 > 1) ? 1 : m23));
                        result._x = Math.asin(-m23clamped);
                        if (Math.abs(m23) < 0.99999) {
                            result._y = Math.atan2(m13, m33);
                            result._z = Math.atan2(m21, m22);
                        } else {
                            result._y = Math.atan2(-m31, m11);
                            result._z = 0;
                        }
                    };
                }
                /******
                END
                ******/
                else {

                   }

            }.bind(this));
        }

        toggleDO() {
            var toggleElement = $('#toggleDeviceOrientation');
            if (this.state.enabled) {
                this.state.controls.disableMethod('deviceOrientation');
                this.setState({
                    enabled: false
                });
                toggleElement.className = '';
            } else {
                this.state.deviceOrientationControlMethod.getPitch(function(err, pitch) {
                    if (!err) {
                        this.state.view.setPitch(pitch);
                    }
                }.bind(this));
                this.state.controls.enableMethod('deviceOrientation');
                this.setState({
                    enabled: true
                });
                toggleElement.className = 'enabled';
            }
        }

        toggleVR() {
            if (!this.state.isVR)
                this.setState({
                    isVR: true
                });
            else
               window.location.href = "/demo";
        }

    render() {
        if(!this.state.isVR)
        return ( 
            <div>
            <div id = "pano" > </div>
            <div id="toggleVR" onClick={this.toggleVR}>
              <img className="enable" src="../../img/toggleVR.png"/>
            </div>
            <div id="toggleDeviceOrientation" onClick={this.toggleDO}>
              <img className="enable" src="../../img/enable.png"/>
              <img className="disable" src="../../img/disable.png"/>
            </div>
            </div>
        );
        else
        return ( 
            <div>
            <div id = "pano" > </div>
            <div id="toggleVR" onClick={this.toggleVR}>
              <img className="enable" src="../../img/toggleVR.png"/>
            </div>
            </div>
        );
    }
}