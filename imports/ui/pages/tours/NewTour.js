import React from 'react';
import TourEditor from '../../components/tours/TourEditor.js';

const NewTour = () => (
  <div className="NewTour">
    <h4 className="page-header">New Tour</h4>
    <TourEditor />
  </div>
);

export default NewTour;
