import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import ToursList from '../../containers/tours/ToursList.js';

const Tours = () => (
  <div className="Tours">
    <Row>
      <Col xs={ 12 }>
        <div className="page-header clearfix">
          <h4 className="pull-left">Tours</h4>
          <Button
            bsStyle="success"
            className="pull-right"
            href="/tours/new"
          >New Tour</Button>
        </div>
        <ToursList />
      </Col>
    </Row>
  </div>
);

export default Tours;
