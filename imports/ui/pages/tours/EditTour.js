import React from 'react';
import TourEditor from '../../components/tours/TourEditor.js';

const EditTour = ({ tour }) => (
  <div className="EditTour">
    <h4 className="page-header">Editing "{ tour.title }"</h4>
    <TourEditor tour={ tour } />
  </div>
);

EditTour.propTypes = {
  tour: React.PropTypes.object,
};

export default EditTour;
