import React from 'react';
import { ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { browserHistory } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { removeTour } from '../../../api/tours/methods.js';

const handleRemove = (_id) => {
  if (confirm('Are you sure? This is permanent!')) {
    removeTour.call({ _id }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Tour deleted!', 'success');
        browserHistory.push('/tours');
      }
    });
  }
};

const ViewTour = ({ tour }) => (
  <div className="ViewTour">
    <div className="page-header clearfix">
      <h4 className="pull-left">{ tour.title }</h4>
      <ButtonToolbar className="pull-right">
        <ButtonGroup bsSize="small">
          <Button href={`/tours/${tour._id}/edit`}>Edit</Button>
          <Button onClick={ () => handleRemove(tour._id) } className="text-danger">Delete</Button>
        </ButtonGroup>
      </ButtonToolbar>
    </div>
    { tour.description }
    <div className="text-center" style={{"marginTop": "20px"}}>
      <p><a className="btn btn-success" href={`/tours/${tour._id}/viewer`} role="button">Virtual Tour</a></p>
    </div>
  </div>
);

ViewTour.propTypes = {
  tour: React.PropTypes.object.isRequired,
};

export default ViewTour;
