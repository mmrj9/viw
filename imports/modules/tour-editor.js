/* eslint-disable no-undef */

import { browserHistory } from 'react-router';
import { Bert } from 'meteor/themeteorchef:bert';
import { upsertTour, upsertTourData } from '../api/tours/methods.js';
import './validation.js';

let component;

const handleUpsert = () => {
  const { tour } = component.props;
  const confirmation = tour && tour._id ? 'Tour updated!' : 'Tour added!';
  const upsert = {
    created_by: Meteor.userId(),
    title: document.querySelector('[name="title"]').value.trim(),
    description: document.querySelector('[id="description"]').value.trim(),
  };

  if (tour && tour._id) upsert._id = tour._id;

  upsertTour.call(upsert, (error, { insertedId }) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      upsertTourData.call({"tourId": insertedId, "files": component.state.files});
      component.tourEditorForm.reset();
      Bert.alert(confirmation, 'success');
      browserHistory.push(`/tours/${insertedId || tour._id}`);
    }
  });
};

const validate = () => {
  if(!Meteor.userId()){
      Bert.alert("You must be logged in.", 'danger');
      browserHistory.push("/login");
      return;
  }
 
  $(component.tourEditorForm).validate({
    rules: {
      title: {
        required: true,
      },
    },
    messages: {
      title: {
        required: 'Need a title in here, Seuss.',
      },
    },
    submitHandler() { handleUpsert(); },
  });
};

export default function tourEditor(options) {
  component = options.component;
  validate();
}
