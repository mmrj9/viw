import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Tours from '../tours';
import TourData from '../tourdata';

Meteor.publish('tours.list', (userId) => {
	check(userId, String);
	if ( Roles.userIsInRole( userId, 'admin' ) )
		return Tours.find();
	else {
		return Tours.find({"created_by": userId});
	}
});

Meteor.publish('tours.view', (_id) => {
  check(_id, String);
  return Tours.find(_id);
});

Meteor.publish('tourdata.bytour', (tourId) => {
  check(tourId, String);
  return TourData.find({"tourId": tourId});
});