import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { Factory } from 'meteor/dburles:factory';

const Tours = new Mongo.Collection('Tours');
export default Tours;

Tours.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Tours.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Tours.schema = new SimpleSchema({
  created_by: {
    type: String,
    label: 'The creator of the tour.',
  },
  title: {
    type: String,
    label: 'The title of the tour.',
  },
  description: {
    type: String,
    label: 'The description of the tour.',
    optional: true,
  },
  isPrivate: {
    type: Boolean,
    label: 'Is the tour private?',
    optional: true,
    defaultValue: false,
  },
  created_at: {
    type: Date,
    label: 'Creation Date of the tour.',
    optional: true,
    defaultValue: new Date(),
  },
  updated_at: {
    type: Date,
    label: 'Last Update of the tour.',
    optional: true,
    defaultValue: new Date(),
  }

});

Tours.attachSchema(Tours.schema);

Factory.define('tour', Tours, {
  title: () => 'Factory Title',
  description: () => 'Factory Description',
});
