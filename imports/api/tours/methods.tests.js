/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { resetDatabase } from 'meteor/xolvio:cleaner';
import { Factory } from 'meteor/dburles:factory';
import Tours from './tours.js';
import { upsertTour, removeTour } from './methods.js';

describe('Tours methods', function () {
  beforeEach(function () {
    if (Meteor.isServer) {
      resetDatabase();
    }
  });

  it('inserts a tour into the Tours collection', function () {
    upsertTour.call({
      title: 'You can\'t arrest me, I\'m the Cake Boss!',
      description: 'They went nuts!',
    });

    const getTour = Tours.findOne({ title: 'You can\'t arrest me, I\'m the Cake Boss!' });
    assert.equal(getTour.description, 'They went nuts!');
  });

  it('updates a tour in the Tours collection', function () {
    const { _id } = Factory.create('tour');

    upsertTour.call({
      _id,
      title: 'You can\'t arrest me, I\'m the Cake Boss!',
      description: 'They went nuts!',
    });

    const getTour = Tours.findOne(_id);
    assert.equal(getTour.title, 'You can\'t arrest me, I\'m the Cake Boss!');
  });

  it('removes a tour from the Tours collection', function () {
    const { _id } = Factory.create('tour');
    removeTour.call({ _id });
    const getTour = Tours.findOne(_id);
    assert.equal(getTour, undefined);
  });
});
