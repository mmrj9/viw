import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import Tours from './tours';
import TourData from './tourdata';
import rateLimit from '../../modules/rate-limit.js';

export const upsertTour = new ValidatedMethod({
    name: 'tours.upsert',
    validate: new SimpleSchema({
        _id: {
            type: String,
            optional: true
        },
        created_by: {
            type: String,
            optional: true
        },
        title: {
            type: String,
            optional: true
        },
        description: {
            type: String,
            optional: true
        },
    }).validator(),
    run(tour) {
        //Assign Default Values
        tour.isPrivate = false;
        var date = new Date()
        tour.created_at = date;
        tour.updated_at = date;
        return Tours.upsert({
            _id: tour._id
        }, {
            $set: tour
        });
    },
});

export function upsertTourData() {
    var existingTourData = TourData.find({
        tourId: this.tourId
    }).fetch()[0];
    var tourdata = {
        "tourId": this.tourId,
        "settings": {
            "mouseViewMode": "drag",
            "autorotateEnabled": false,
            "fullscreenButton": true,
            "viewControlButtons": false
        }
    };

    var scenes = [];
    var nFiles = this.files.length;
    var file = this.files[0];

    S3.upload({
            files: this.files,
            path: "tours/" + this.tourId
        }, function(e, r) {
            //Asyncrohnous method to determine image width
            //create image element
            var img = document.createElement('img');
            //Assign onload function
            img.onload = function() {
              console.log("onload");
                var scene = {
                    //take the .jpg out of the id
                    "id": r.file.name.substr(0, r.file.name.indexOf(".")),
                    //take the .jpg out of the name and replace "-" for " "
                    "name": r.file.original_name.substr(0, r.file.original_name.indexOf(".")).replace(/-/g, " "),
                    "levels": [{
                        tileSize: this.width,
                        size: this.width
                    }],
                    "width": this.width,
                    "initialViewParameters": {
                        "pitch": 0,
                        "yaw": 0,
                        "fov": 1.5707963267948966
                    },
                    "linkHotspots": [],
                    "infoHotspots": []
                }
                scenes.push(scene);
                nFiles -= 1;
                console.log(nFiles);
                if (nFiles == 0) {
                    tourdata.scenes = scenes;
                    if (existingTourData)
                        return TourData.upsert({
                            _id: existingTourData._id
                        }, {
                            $set: tourdata
                        });
                    else{
                        console.log(tourdata);
                        return TourData.insert(tourdata);
                      }
                }
            };
        //element to load
        img.src = r.url;
    });

}

export const removeTour = new ValidatedMethod({
    name: 'tours.remove',
    validate: new SimpleSchema({
        _id: {
            type: String
        },
    }).validator(),
    run({
        _id
    }) {
        Tours.remove(_id);
    },
});

rateLimit({
    methods: [
        upsertTour,
        removeTour,
    ],
    limit: 5,
    timeRange: 1000,
});