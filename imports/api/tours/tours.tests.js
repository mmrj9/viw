/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

import { assert } from 'meteor/practicalmeteor:chai';
import Tours from './tours.js';

describe('Tours collection', function () {
  it('registers the collection with Mongo properly', function () {
    assert.equal(typeof Tours, 'object');
  });
});
