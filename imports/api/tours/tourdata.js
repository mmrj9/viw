import { Mongo } from 'meteor/mongo';

const TourData = new Mongo.Collection('TourData');
export default TourData;

TourData.allow({
  insert: () => function(userId){
    return userId != null;
  },
  update: () => false,
  remove: () => false,
});

TourData.deny({
  insert: () => false,
  update: () => true,
  remove: () => true,
});