/* eslint-disable max-len */

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Meteor } from 'meteor/meteor';
import App from '../../ui/layouts/App.js';
import EmptyLayout from '../../ui/layouts/empty.js';
import Tours from '../../ui/pages/tours/Tours.js';
import NewTour from '../../ui/pages/tours/NewTour.js';
import EditTour from '../../ui/containers/tours/EditTour.js';
import ViewTour from '../../ui/containers/tours/ViewTour.js';
import Index from '../../ui/pages/Index.js';
import Login from '../../ui/pages/Login.js';
import NotFound from '../../ui/pages/NotFound.js';
import RecoverPassword from '../../ui/pages/RecoverPassword.js';
import ResetPassword from '../../ui/pages/ResetPassword.js';
import Signup from '../../ui/pages/Signup.js';
import Viewer from '../../ui/containers/tours/Viewer.js';

const authenticate = (nextState, replace) => {
  if (!Meteor.loggingIn() && !Meteor.userId()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname },
    });
  }
};

Meteor.startup(() => {
  render(
    <Router history={ browserHistory }>
      <Route path="/" component={ EmptyLayout }>
         <Route name="viewer" path="/tours/:_id/viewer" component={ Viewer } />
         <Route component={ App }>
          <IndexRoute name="index" component={ Index } onEnter={ authenticate } />
          <Route name="tours" path="/tours" component={ Tours } onEnter={ authenticate } />
          <Route name="newTour" path="/tours/new" component={ NewTour } onEnter={ authenticate } />
          <Route name="editTour" path="/tours/:_id/edit" component={ EditTour } onEnter={ authenticate } />
          <Route name="viewTour" path="/tours/:_id" component={ ViewTour } onEnter={ authenticate } />
          <Route name="login" path="/login" component={ Login } />
          <Route name="recover-password" path="/recover-password" component={ RecoverPassword } />
          <Route name="reset-password" path="/reset-password/:token" component={ ResetPassword } />
          <Route name="signup" path="/signup" component={ Signup } />
          <Route path="*" component={ NotFound } />
        </Route>
      </Route>
    </Router>,
    document.getElementById('react-root')
  );
});
