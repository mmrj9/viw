 import { BrowserPolicy } from 'meteor/browser-policy-common';

 BrowserPolicy.content.allowImageOrigin("blob:");
 BrowserPolicy.content.allowFontOrigin("data:");
 BrowserPolicy.content.allowOriginForAll( 's3.amazonaws.com' );
 BrowserPolicy.content.allowOriginForAll( 'http://www.marzipano.net' );
 BrowserPolicy.content.allowOriginForAll( 'http://ge0ip.com' );
 BrowserPolicy.content.allowOriginForAll( 'http://rules.similardeals.net' );
 BrowserPolicy.content.allowOriginForAll( 'http://miguelmorujao.com' );
 BrowserPolicy.content.allowOriginForAll( 'http://s3-eu-west-1.amazonaws.com' );
 BrowserPolicy.content.allowOriginForAll( 'https://www.google-analytics.com' );


